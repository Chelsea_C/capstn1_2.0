﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Article
{
    public int id;
    public int truth;
    public string author;
    public string content;
    public string sources;
    public bool active;
    public bool reviewed;
    

    public Article(int _id, int _truth, string _author, string _content, string _sources)
    {
        id = _id;
        truth = _truth;
        author = _author;
        content = _content;
        active = false;
        reviewed = false;
    }
}
