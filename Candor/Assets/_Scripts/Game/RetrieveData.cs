﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mono.Data.Sqlite;
using System.Data;
using System;

namespace DATABASES
{
    public class RetrieveData : MonoBehaviour
    {
        public GameData database;

        void Start()
        {
            database.ClearAll();

            //Debug.Log(Application.dataPath + "/CandorDB.s3db");
            string conn = "URI=file:" + Application.dataPath + "/Resources/SQL/CandorDB.s3db";
            IDbConnection dbconn;
            dbconn = (IDbConnection)new SqliteConnection(conn);
            dbconn.Open();

            GetDatabaseEntries(dbconn);
            GetArticles(dbconn);
            GetAuthors(dbconn);

            dbconn.Close();
            dbconn = null;
        }

        void GetDatabaseEntries(IDbConnection dbconn)
        {
            IDbCommand dbcmd = dbconn.CreateCommand();

            string sqlQuery = "SELECT IFNULL(Source, ''), IFNULL(Keywords, ''), IFNULL(Content, '') " + "FROM DATABASE";
            dbcmd.CommandText = sqlQuery;
            IDataReader reader = dbcmd.ExecuteReader();

            while (reader.Read())
            {
                string source = reader.GetString(0);
                string keywords = reader.GetString(1);
                string content = reader.GetString(2);

                DatabaseEntry entry = new DatabaseEntry(source, keywords, content);
                database.entries.Add(entry);
            }

            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
        }

        void GetArticles(IDbConnection dbconn)
        {
            IDbCommand dbcmd = dbconn.CreateCommand();

            string sqlQuery = "SELECT Id, Truth, IFNULL(Author, ''), IFNULL(Content, ''), IFNULL(Sources, '') " + "FROM ARTICLES";
            dbcmd.CommandText = sqlQuery;
            IDataReader reader = dbcmd.ExecuteReader();

            while (reader.Read())
            { 
                int id = reader.GetInt32(0);
                int truth = reader.GetInt32(1);
                string author = reader.GetString(2);
                string content = reader.GetString(3);
                string sources = reader.GetString(4);

                Article article = new Article(id, truth, author, content, sources);
                database.articles.Add(article);
            }

            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
        }

        void GetAuthors(IDbConnection dbconn)
        {
            IDbCommand dbcmd = dbconn.CreateCommand();

            string sqlQuery = "SELECT Name, PortraitPath " + "FROM AUTHORS";
            dbcmd.CommandText = sqlQuery;
            IDataReader reader = dbcmd.ExecuteReader();

            while (reader.Read())
            {
                //string name = " ";
                //string portraitPath = " ";

                //if (reader.GetString(0) != null)
                  string  name = reader.GetString(0);
                //if (reader.GetString(1) != null)
                   string portraitPath = reader.GetString(1);

                Sprite portrait = Resources.Load<Sprite>("Authors/" + portraitPath);

                Author author = new Author(name, portrait);
                database.authors.Add(author);
            }

            reader.Close();
            reader = null;
            dbcmd.Dispose();
            dbcmd = null;
        }
    }
}
