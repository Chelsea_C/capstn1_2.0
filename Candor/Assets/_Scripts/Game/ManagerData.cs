﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New ManagerData", menuName = "ScriptableObject/ManagerData")]
public class ManagerData : ScriptableObject
{
    public GameObject audioMgr;
    public GameObject windowMgr;
    public GameObject emailMgr;
    public GameObject inputMgr;
    public GameObject authorMgr;
    public GameObject levelMgr;
    public GameObject articleMgr;
    public GameObject databaseMgr;
}
