﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DATABASES;

[CreateAssetMenu(fileName = "New GameData", menuName = "ScriptableObject/GameData")]
public class GameData : ScriptableObject
{
    public List<Author> authors;
    public List<Article> articles;
    public List<DatabaseEntry> entries;

    public void ClearAll()
    {
        authors.Clear();
        articles.Clear();
        entries.Clear();
    }
}
