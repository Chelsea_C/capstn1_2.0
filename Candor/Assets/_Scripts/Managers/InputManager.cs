﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InputManager : MonoBehaviour
{
    #region Singleton
    public static InputManager instance;

    void Awake()
    {
        instance = this;
    }
    #endregion

    public string pauseScene;
    public List<GameObject> interactables;

    public bool isPaused;

    void Start()
    {
        isPaused = false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused)
            {
                Resume();
                return;
            }
            isPaused = true;
            SceneManager.LoadScene(pauseScene, LoadSceneMode.Additive);
        }

        if (isPaused)
        {
            FindAllInteractables();
            Pause();
        }
        else
            Resume();
    }

    void FindAllInteractables()
    {
        GameObject[] objects = GameObject.FindGameObjectsWithTag("Interactable");
        foreach (GameObject go in objects)
        {
            interactables.Add(go);
        }
    }

    void Pause()
    {
        Time.timeScale = 0;
        foreach (GameObject go in interactables)
            SetLayersRecursively(go.transform, 2);
            //go.layer = LayerMask.NameToLayer("IgnoreRaycast");
    }

    void Resume()
    {
        Time.timeScale = 1;
        foreach (GameObject go in interactables)
            SetLayersRecursively(go.transform, 0);
            //go.layer = LayerMask.NameToLayer("IgnoreRaycast");
    }

    void SetLayersRecursively(Transform trans, int index)
    {
        trans.gameObject.layer = index;
        foreach(Transform child in trans)
        {
            SetLayersRecursively(child, index);
        }
    }
}

