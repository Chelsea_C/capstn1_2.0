﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    #region Singleton
    public static AudioManager instance;

    void Awake()
    {
        instance = this;
    }
    #endregion
    
    [Header("Background Music")]
    public AudioSource bgm;
    public AudioClip bgmClip;

    [Header("Mouse Click")]
    public AudioSource click;
    public AudioClip clickClip;

    [Header("Notification")]
    public AudioSource notif;
    public AudioClip notifClip;

    [Header("End Level")]
    public AudioSource end;
    public AudioClip endClip;

    [Header("Success")]
    public AudioSource success;
    public AudioClip successClip;
    
    void Start()
    {
        PairAudioClips();
        bgm.loop = true;
        bgm.Play();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
            click.Play();
    }

    void PairAudioClips()
    {
        bgm.clip = bgmClip;
        click.clip = clickClip;
        notif.clip = notifClip;
        end.clip = endClip;
        success.clip = successClip;
    }

    public void BGMMute(bool mute)
    {
        bgm.mute = mute;
    }

    public void SFXMute(bool mute)
    {
        click.mute = mute;
        notif.mute = mute;
    }
}
