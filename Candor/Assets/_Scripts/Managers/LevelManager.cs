﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[System.Serializable]
public class LevelSummary
{
    public float time_remaining;
    public int change_score;
    public int end_score;
    public int totalProcessed;
    public int correct_approvals;
    public int correct_rejections;
    public int incorrect_approvals;
    public int incorrect_rejections;
}

[System.Serializable]
public class LevelInfo
{
    public float time;
    public int fakeArticlesCount;
    public int realArticlesCount;
    public int numTasks;
    public int difficulty;
}

public class LevelManager : MonoBehaviour
{
    #region Singleton
    public static LevelManager instance;

    void Awake()
    {
        instance = this;
    }
    #endregion

    [Header("Level Info")]
    public LevelInfo[] levels;
    public Timer timer;

    [Header("Current Articles")]
    public List<Article> currentArticles;

    [Header("Active Articles")]
    public List<Article> activeArticles;

    [SerializeField]
    int currentLevel;

    
    public int CurrentLevel { get { return this.currentLevel; } set { this.currentLevel = value; } }

    void Start()
    {
        timer = GetComponent<Timer>();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.X))
        {
            CurrentLevel = 1;
            StartLevel();
            Debug.Log("Articles generated. Current Level: " + CurrentLevel);            
        }

        if (Input.GetKeyDown(KeyCode.Y))
        {
            PostManager.instance.RemovePost(currentArticles[0].id);
        }

        if (InputManager.instance.isPaused)
        {

        }
    }

    #region Level Setup
    public void StartLevel()
    {
        currentArticles.Clear();
        SetTimer(levels[currentLevel].time);
        AddArticles();
    }
    
    void SetTimer(float countdown)
    {
        timer.countdownValue = (long)countdown;
        timer.ResetCountdown();

    }

    public void AddTutorialArticle(Article a)
    {
        currentArticles.Clear();
        currentArticles.Add(a);
        PublishCandorTickets();
        PublishHeadnotePosts();
    }

    void AddArticles()
    {
        var databaseArticles = DATABASES.DatabaseManager.instance.database.articles;
        var currentLevelInfo = levels[currentLevel];

        int realQuota = currentLevelInfo.realArticlesCount;
        int fakeQuota = currentLevelInfo.fakeArticlesCount;

        int availableArticles = 0;
        foreach (Article a in databaseArticles)
            if (!a.active)
                availableArticles++;

        if (availableArticles < (fakeQuota + realQuota) || availableArticles <= 0)
        {
            Debug.Log("Not enough articles remaining");
            return;
        }

        int realCount = 0;
        int fakeCount = 0;
        bool quotaReached = false;

        while (!quotaReached)
        {
            int randomIndex = Random.Range(0, databaseArticles.Count);
            var article = databaseArticles[randomIndex];
            Debug.Log(article.id);

            if (!article.active)
            {
                if (realCount != realQuota && article.truth == 1)
                {
                    currentArticles.Add(article);
                    article.active = true;
                    realCount++;
                }

                if (fakeCount != fakeQuota && article.truth == 0)
                {
                    currentArticles.Add(databaseArticles[randomIndex]);
                    article.active = true;
                    fakeCount++;
                }
            }

            if (realCount == realQuota && fakeCount == fakeQuota)
                quotaReached = true;
        }

        for (int i = 0; i < databaseArticles.Count; i++)
        {
            if (databaseArticles[i].active)
                activeArticles.Add(databaseArticles[i]);
        }

        PublishCandorTickets();
        PublishHeadnotePosts();
    }

    void PublishHeadnotePosts()
    {
        foreach (Article active in currentArticles)
            PostManager.instance.CreatePost(active);
    }

    void PublishCandorTickets()
    {
        foreach (Article active in currentArticles)
            TicketManager.instance.CreateTicket(active);
    }
    #endregion

}
