﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public long countdownValue = 120;
    public long currCountdownValue;
    public UnityEngine.UI.Text countdownText;

    public bool timerActive = false;

    void Update()
    {
        if (Input.GetMouseButton(0) && !timerActive)
        {
            timerActive = true;
            StartTimer();
        }

        if(timerActive)
            UpdateTimerDisplay();


    }

    void UpdateTimerDisplay()
    {
        var elapsedMins = currCountdownValue / 60;
        var elapsedSecs = currCountdownValue % 60;

        string timeText = string.Format("{0:D1}:{1:D2}", elapsedMins, elapsedSecs);
        countdownText.text = timeText;
    }

    public void ResetCountdown()
    {
        currCountdownValue = countdownValue;
    }

    public void StartTimer()
    {
        StartCoroutine(StartCountdown());
    }
    
    public IEnumerator StartCountdown()
    {
        currCountdownValue = countdownValue;
        while (currCountdownValue > 0)
        {
            //Debug.Log("Countdown: " + currCountdownValue);
            yield return new WaitForSeconds(1.0f);
            currCountdownValue--;
        }
    }
}
