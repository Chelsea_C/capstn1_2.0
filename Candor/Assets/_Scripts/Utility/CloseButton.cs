﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CloseButton : MonoBehaviour, IPointerEnterHandler
{

    public void OnPointerEnter(PointerEventData eventData)
    {
        WindowManager.instance.DisableWindowActivators();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        WindowManager.instance.EnableWindowActivators();
    }

}
