﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PostUI : MonoBehaviour
{
    public Article article;
    public int postID;
    public Author author;
    public Image authorPortrait;
    public TMP_Text datetime;
    public TMP_Text authorName;
    public TMP_Text content;

    public TMP_Text edited;

    [Header("Comments")]
    public GameObject commentsParent;

    public void AddArticle(Article _article)
    {
        article = _article;
        postID = _article.id;
        author = AuthorManager.instance.GetAuthor(_article.author);
        authorPortrait.sprite = author.portrait;
        authorName.text = author.name;
        content.text = article.content;
    }

    public void Remove()
    {
        Destroy(gameObject);
    }

}
