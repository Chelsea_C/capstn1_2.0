﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostManager : MonoBehaviour
{
    #region Singleton
    public static PostManager instance;

    void Awake()
    {
        instance = this;
    }
    #endregion

    public GameData database;

    public GameObject postUIPrefab;
    public GameObject postsParent;
    public float spacing;

    public GameObject commentUIPrefab;

    public List<PostUI> currentPosts;
    public List<PostUI> activePosts;

    public void CreatePost(Article a)
    {
        GameObject post = Instantiate(postUIPrefab, postsParent.transform);
        PostUI ui = post.GetComponent<PostUI>();
        ui.article = a;
        ui.postID = a.id;
        ui.author = AuthorManager.instance.GetAuthor(a.author);
        ui.authorPortrait.sprite = ui.author.portrait;
        ui.authorName.text = ui.author.name;
        ui.datetime.text = "Just Now";
        ui.content.text = a.content;

        if (a.sources != " ")
        {
            ui.content.text += "$$Source: " + a.sources;
            ui.content.text = ui.content.text.Replace('$', '\n');
        }

        currentPosts.Add(ui);

        ResetPostParentTransform();
    }

    public void ResetPostParentTransform()
    {
        UnityEngine.UI.LayoutRebuilder.ForceRebuildLayoutImmediate(postsParent.GetComponent<RectTransform>());
    }

    public void UpdatePost(int id, string edit)
    {
        
    }

    public void AddComment(int _postID)
    {
        for(int i = 0; i < currentPosts.Count; i++)
        {
            if(currentPosts[i].postID == _postID)
            {

            }
        }
    }

    public void RemovePost(int _postID)
    {
        for(int i = 0; i < currentPosts.Count; i++)
            if (currentPosts[i].postID == _postID)
                currentPosts[i].Remove();
    }
}
