﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    public AudioSource BGM;
    public AudioSource mouseClicker;
    public AudioClip bgmClip;
    public AudioClip mouseClickClip;

    void Start()
    {
        BGM.clip = bgmClip;
        BGM.loop = true;
        BGM.Play();

        mouseClicker.clip = mouseClickClip;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
            mouseClicker.Play();
    }
}
