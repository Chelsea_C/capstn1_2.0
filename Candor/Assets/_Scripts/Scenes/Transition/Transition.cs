﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transition : MonoBehaviour
{
    public string nextScene;
    public Color loadToColor = Color.white;

    public void TransitionToNextScene()
    {
        Initiate.Fade(nextScene, loadToColor, 1.0f);
    }
}
