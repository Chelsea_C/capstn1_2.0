﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

//[System.Serializable]
//public class SceneInfo
//{
//    public string previousScene;
//    public string current;
//    public string nextScene;
//}

public class SceneHandler : MonoBehaviour
{
    //public List<SceneInfo> scenes;
    //public Scene transition;

    void Update()
    {
        //currentScene = SceneManager.GetActiveScene().name;
    }

    //public void GoToNextScene(string name)
    //{
    //    for(int i = 0; i < scenes.Count; i++)
    //    {
    //        if (scenes[i].current == name)
    //            Initiate.Fade(scenes[i].nextScene, Color.black, 1);
    //    }
    //}

    public void GoToScene(string name)
    {
        //SceneManager.LoadScene(name);
        Initiate.Fade(name, Color.black, 1);
    }

    public void GoToScene(int index)
    {
        string name = SceneManager.GetSceneAt(index).name;
        //SceneManager.LoadScene(index);
        Initiate.Fade(name, Color.black, 1);
    }

    public void Quit()
    {
        Debug.Log("Exiting game");
        Application.Quit();
    }

    public void RemoveScene(string name)
    {
        SceneManager.UnloadSceneAsync(name);
    }

    public void RemoveScene(int index)
    {
        SceneManager.UnloadSceneAsync(index);
    }

    public void LoadScene(string name)
    {
        Scene scene = SceneManager.GetSceneByName(name);
        if((scene != null) && !scene.isLoaded)
            SceneManager.LoadScene(name, LoadSceneMode.Additive);
    }
}

