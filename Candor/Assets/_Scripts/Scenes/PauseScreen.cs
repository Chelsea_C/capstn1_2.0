﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseScreen : MonoBehaviour
{

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (InputManager.instance.isPaused)
                Resume();
        }
    }

    public void SaveGame()
    {
        Player.instance.Save();
    }

    public void Resume()
    {
        SceneManager.UnloadSceneAsync("Pause");
        InputManager.instance.isPaused = false;
    }

    public void QuitToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void MuteMusic()
    {
        if(AudioManager.instance.bgm.mute)
            AudioManager.instance.BGMMute(false);
        if (!AudioManager.instance.bgm.mute)
            AudioManager.instance.BGMMute(true);
    }

    public void MuteSFX()
    {
        if (AudioManager.instance.click.mute && AudioManager.instance.notif.mute)
            AudioManager.instance.SFXMute(false);
        if (!AudioManager.instance.click.mute && AudioManager.instance.notif.mute)
            AudioManager.instance.SFXMute(true);
    }
}
