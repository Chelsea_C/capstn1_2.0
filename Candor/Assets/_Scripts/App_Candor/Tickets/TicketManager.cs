﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TicketManager : MonoBehaviour
{
    #region Singleton
    public static TicketManager instance;

    void Awake()
    {
        instance = this;
    }
    #endregion
    public GameData database;

    public List<TicketUI> currentTickets;

    public GameObject ticketUIPrefab;
    public GameObject ticketsParent;

    public void CreateTicket(Article a)
    {
        ResetTicketParentTransform();

        GameObject flagged = Instantiate(ticketUIPrefab, ticketsParent.transform);
        TicketUI fui = flagged.GetComponent<TicketUI>();
        fui.article = a;
        fui.author.text = a.author;
        fui.authorPortrait.sprite = AuthorManager.instance.GetAuthor(a.author).portrait;
        fui.content.text = a.content;
        if (a.sources != " ")
        {
            fui.content.text += "$$Source: " + a.sources;
            fui.content.text = fui.content.text.Replace('$', '\n');
        }

        currentTickets.Add(fui);

        ResetTicketParentTransform();
    }

    void ResetTicketParentTransform()
    {
        UnityEngine.UI.LayoutRebuilder.ForceRebuildLayoutImmediate(ticketsParent.GetComponent<RectTransform>());
    }

    #region Check Player Evaluation

    public void CheckPlayerEvaluation(CheckedArticle chArticle)
    {
        //If player evaluation was correct
        if (chArticle.correct)
        {
            //and article was fake
            if(chArticle.article.truth == 0)
            {
                //PostManager.instance.
            }
            //and article was true
            if (chArticle.article.truth == 1)
            {

            }
        }
        
        //If player evaluation was incorrect
        if(!chArticle.correct)
        {
            //and article was fake
            if(chArticle.article.truth == 0)
            {

            }
            //and article was true
            if(chArticle.article.truth == 1)
            {

            }
        }

    }

    #endregion
}
