﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TicketUI : MonoBehaviour
{
    public Article article;
    public int articleID;
    public Image authorPortrait;
    public TMP_Text author;
    public TMP_Text content;
    public TMP_Text status;
    public Sprite disabledButtonSprite;

    [SerializeField]
    Button[] buttons;

    void Start()
    {
        status.text = "Status: Unchecked";
        buttons = GetComponentsInChildren<Button>();
    }

    public void AddArticle(Article _article)
    {
        article = _article;
        articleID = _article.id;
        author.text = _article.author;
        authorPortrait.sprite = AuthorManager.instance.GetAuthor(_article.author).portrait;
        content.text = _article.content;

        if (_article.sources != " ")
        {
            content.text += "$$Source: " + _article.sources;
            content.text = content.text.Replace('$', '\n');
        }
    }

    public void Approve()
    {
        if(article == null)
        {
            Debug.Log("No article found");
            return;
        }

        Debug.Log("Approved article");
        SetStatus("APPROVED");
        Player.instance.ApproveArticle(article);
    }

    public void Reject()
    {
        if (article == null)
        {
            Debug.Log("No article found");
            return;
        }

        Debug.Log("Rejected article");
        SetStatus("REJECTED");
        Player.instance.RejectArticle(article);
    }

    public void Opinion()
    {
        if (article == null)
        {
            Debug.Log("No article found");
            return;
        }

        Debug.Log("Opinionated article");
        SetStatus("OPINION");
        Player.instance.MarkArticleAsOpinion (article);
    }

    void SetStatus(string _status)
    {
        status.text = "Status: " + _status;
        DisableButtons();
    }

    void DisableButtons()
    {
        for(int i = 0; i < buttons.Length; i++)
        {
            buttons[i].enabled = false;
            bool matches = status.text.Contains(buttons[i].name, StringComparison.OrdinalIgnoreCase);
            if (matches)
                return;
            buttons[i].GetComponent<Image>().sprite = disabledButtonSprite;
        }
    }
}
