﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EmailPopoutUI : Window
{
    public EmailUI ui;
    public TMP_Text sender;
    public TMP_Text subject;
    public TMP_Text content;

    public Image portrait;

    public void DisplayEmail(EmailUI _ui)
    {
        ui = _ui;
        sender.text = "Sender: " + ui.email.sender;
        subject.text = "Subject: " + ui.email.subject;
        content.text = ui.email.content;
        portrait.sprite = ui.email.portrait;
    }

    public void ClosePopup()
    {
        WindowManager.instance.windows.Remove(this);
        ui.CloseEmail();
        Destroy(gameObject);
    }
}
