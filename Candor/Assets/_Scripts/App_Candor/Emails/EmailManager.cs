﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmailManager : MonoBehaviour
{
    #region Singleton
    public static EmailManager instance;

    void Awake()
    {
        instance = this;
    }
    #endregion



    public GameObject emailUIPrefab;
    public GameObject emailPopoutPrefab;
    public GameObject emailsPanel;

    //bool trigger;

    void Start()
    {
        //trigger = true;
    }

    void Update()
    {
        ////TESTING
        //if (Input.GetKeyDown(KeyCode.X) && trigger)
        //{
        //    //CreateEmail("Lillian Holloway", "Female6", "Welcome to Candor",
        //    //    "Greetings! We are excited to have you on the team and hope you're settling in nicely." +
        //    //    "For your first day, we'll just have you do a few things to get you familiarized with the" +
        //    //    "company platform.\n First, see if you can find your list of assignments in the sidebar.");
        //    trigger = false;
        //}
    }


    public void CreateEmail(Author author, string _subject, string _content)
    {
        Email email = new Email(author, _subject, _content);
        Player.instance.emails.Add(email);
        DisplayEmail(email);
    }

    public void CreateEmail(string _sender, string _portraitPath, string _subject, string _content)
    {
        Email email = new Email(_sender, _portraitPath, _subject, _content);
        Player.instance.emails.Add(email);
        DisplayEmail(email);
    }

    public void DisplayEmail(Email email)
    {
        GameObject emailObj = Instantiate(emailUIPrefab, emailsPanel.transform);
        emailObj.transform.SetAsFirstSibling();
        EmailUI ui = emailObj.GetComponent<EmailUI>();
        ui.AddEmail(email);
    }

    public Email GetEmail(string subject)
    {
        for(int i = 0; i < Player.instance.emails.Count; i++)
        {
            if (Player.instance.emails[i].subject.Contains(subject, System.StringComparison.OrdinalIgnoreCase))
                return Player.instance.emails[i];
        }
        return null;
    }

}
