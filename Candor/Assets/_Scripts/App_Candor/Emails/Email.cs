﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Response
{
    Negative,
    Positive
}

[System.Serializable]
public class Email
{
    public string sender;
    public string subject;
    public string content;
    public Sprite portrait;
    public bool read;

    public Email(Author author, string _subject, string _content)
    {
        sender = author.name;
        subject = _subject;
        content = _content;
        portrait = author.portrait;
        read = false;
    }

    public Email(string _sender, string _portraitPath, string _subject, string _content)
    {
        sender = _sender;
        portrait = Resources.Load<Sprite>("Authors/" + _portraitPath);
        subject = _subject;
        content = _content;
        read = false;
    }
}
