﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EmailUI : MonoBehaviour
{
    public Email email;
    public Image portrait;
    public TMP_Text sender;
    public TMP_Text subject;

    GameObject emailPopout;
    bool popoutActive;

    void Start()
    {
        popoutActive = false;
    }

    public void AddEmail(Email _email)
    {
        email = _email;
        portrait.sprite = email.portrait;
        sender.text = "Sender: " + email.sender;
        subject.text = "Subject: " + email.subject;
    }

    public void OpenEmail()
    {
        if (popoutActive)
        {
            LeanTween.move(emailPopout, new Vector3(0, 0, 0), 0f).setEase(LeanTweenType.linear);
            WindowManager.instance.SetActiveWindow(emailPopout.GetComponent<EmailPopoutUI>());
            return;
        }

        if (!email.read)
        {
            email.read = true;
            GetComponent<Image>().color = Color.gray;
        }
        
        popoutActive = true;

        emailPopout = Instantiate(EmailManager.instance.emailPopoutPrefab);

        var popoutWindow = emailPopout.GetComponent<EmailPopoutUI>();
        popoutWindow.DisplayEmail(this);
       
        WindowManager.instance.windows.Add(popoutWindow);
        WindowManager.instance.SetActiveWindow(popoutWindow);

        LeanTween.move(emailPopout, new Vector3(0, 0, 0), 0f).setEase(LeanTweenType.linear);
    }

    public void CloseEmail()
    {
        popoutActive = false;
    }
}
