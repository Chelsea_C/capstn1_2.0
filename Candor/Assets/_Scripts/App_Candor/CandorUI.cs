﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CandorUI : MonoBehaviour
{
    public Transform displayPosition;
    public GameObject alertIndicator;
    public GameObject emailIndicator;

    [Header("Profile")]
    public new TMP_Text name;
    public TMP_Text rank;
    public Image portrait;
    public TMP_Text reviewedArticlesCount;
    public TMP_Text fakeArticlesFound;
    public TMP_Text realArticlesFound;
    public TMP_Text score;

    public List<GameObject> panels;
    
    public GameObject activePanel;

    [SerializeField]
    List<Vector3> startingPositions;

    void Start()
    {
        startingPositions = new List<Vector3>();

        HideIndicator();
        foreach (GameObject go in panels)
        {
            startingPositions.Add(go.transform.localPosition);
        }

        name.text = Player.instance.playerName;
        rank.text = Player.instance.rank.ToString();
        portrait.sprite = Player.instance.playerPortrait;
    }
    
    void Update()
    {
        if (activePanel == null)
            activePanel = panels[0];

        if (Player.instance.hasUnreadEmail())
            ShowIndicator();
        else
            HideIndicator();

        score.text = Player.instance.score.ToString();
        reviewedArticlesCount.text = "Total Articles Reviewed: " + Player.instance.checkedArticles.Count;
        fakeArticlesFound.text = "Percent of False Information Debunked: " + Player.instance.falsePercent.ToString() + "%";
        realArticlesFound.text = "Percent of Legitimate News Confirmed: " + Player.instance.truePercent.ToString() + "%";

    }

    public void ShowPanel(int index)
    {
        LeanTween.move(panels[index], displayPosition.position, 0f).setEase(LeanTweenType.linear);
    }

    public void HidePanel(int index)
    {
        LeanTween.move(panels[index], startingPositions[index], 0f).setEase(LeanTweenType.linear);
    }

    public void SetActivePanel(int index)
    {
        activePanel = panels[index];
        for (int i = 0; i < panels.Count; i++)
        {
            if (i == index)
                ShowPanel(i);
            else
                HidePanel(i);
        }
    }

    public void ShowIndicator()
    {
        LeanTween.alpha(alertIndicator, 1, 0f);
        emailIndicator.SetActive(true);
    }

    public void HideIndicator()
    {
        LeanTween.alpha(alertIndicator, 0, 0f);
        emailIndicator.SetActive(false);
    }

}
