﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class AuthorManager : MonoBehaviour
{
    #region Singleton
    public static AuthorManager instance;

    void Awake()
    {
        instance = this;
    }
    #endregion

    public GameData database;
    List<Author> authorPool;

    void Start()
    {
        authorPool = database.authors;
    }

    public Author GetAuthor(string name)
    {
        List<Author> results = new List<Author>();

        for (int i = 0; i < authorPool.Count; i++)
        {
            bool contains = authorPool[i].name.Contains(name, StringComparison.OrdinalIgnoreCase);
            if (contains)
                results.Add(authorPool[i]);
        }

        if (results.Count == 0)
        {
            Debug.Log("None found");
            return null;
        }
        else
            return results[0];
    }
}
