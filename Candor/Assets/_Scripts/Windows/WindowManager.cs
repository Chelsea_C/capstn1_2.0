﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WindowManager : MonoBehaviour
{
    #region Singleton
    public static WindowManager instance;

    void Awake()
    {
        instance = this;
    }
    #endregion

    [Header("Windows")]
    public List<Window> windows;
    public Window activeWindow;

    [Header("Notifications")]
    public GameObject notificationWindow;

    public Vector3 popupStartPosition;
    public Vector3 popupEndPosition;
    public LeanTweenType entranceEaseType;
    public float animTime = 0.5f;
    public float autoCloseAfterTime = 2f;
    public LeanTweenType exitEaseType;

    void Update()
    {
        ////Testing
        //if (Input.GetKeyDown(KeyCode.X))
        //    CreatePopupNotificationWindow("Hello", "This is a test popup notification!");

        //foreach (Window w in windows)
        //{
        //    if (w == activeWindow)
        //    {
        //        w.GetComponentInChildren<Canvas>().sortingOrder++;
        //    }
        //    else
        //        w.GetComponentInChildren<Canvas>().sortingOrder--;
        //}

    }

    public void SetActiveWindow(Window window)
    {
        activeWindow = window;
        for (int i = 0; i < windows.Count; i++)
        {
            if (windows[i] == activeWindow)
            {
                var movedWindow = windows[i];
                windows.RemoveAt(i);
                windows.Add(movedWindow);
            }

            if (windows[i].GetComponentInChildren<Canvas>() != null)
                windows[i].GetComponentInChildren<Canvas>().sortingOrder = i;
        }
    }

    public void CreatePopupNotificationWindow(string _header, string _message)
    {
        if (notificationWindow == null)
        {
            Debug.LogError("Notif window not set in WindowManager");
            return;
        }
        AudioManager.instance.notif.Play();
        GameObject notifWindow = Instantiate(notificationWindow);
        notifWindow.transform.position = popupStartPosition;

        Notification notif = notifWindow.GetComponent<Notification>();
        notif.header.text = _header;
        notif.message.text = _message;

        LeanTween.move(notifWindow.gameObject, popupEndPosition, animTime).setEase(entranceEaseType);
        AudioManager.instance.notif.Play();

        notif.StartAutoCloseTimer(autoCloseAfterTime);
    }

    public void DisableWindowActivators()
    {
        for(int i = 0; i < windows.Count; i++)
        {
            windows[i].gameObject.GetComponentInChildren<ActivateWindow>().isActivatable = false;
        }
    }

    public void EnableWindowActivators()
    {

        for (int i = 0; i < windows.Count; i++)
        {
            windows[i].gameObject.GetComponentInChildren<ActivateWindow>().isActivatable = true;
        }
    }

}
