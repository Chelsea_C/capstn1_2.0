﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Notification : Window
{
    public TMP_Text message;
    public TMP_Text header;

    void Start()
    {
        moveable = false;
    }

    public void StartAutoCloseTimer(float time)
    {
        Invoke("CloseNotification", time);
    }

    public void CloseNotification()
    {
        LeanTween.move(gameObject, WindowManager.instance.popupStartPosition, 1f).setEase(WindowManager.instance.exitEaseType);
        Invoke("RemovePopup", 3f);
    }

    void RemovePopup()
    {
        Destroy(gameObject);
    }
}
