﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class ActivateWindow : MonoBehaviour
{
    Window window;
    public bool isActivatable;

    void Start()
    {
        isActivatable = true;
        window = GetComponentInParent<Window>();
    }

    void OnMouseDown()
    {
        if(isActivatable)
            WindowManager.instance.SetActiveWindow(window);
    }

}
