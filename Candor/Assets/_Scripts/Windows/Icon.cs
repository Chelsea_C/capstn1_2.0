﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[RequireComponent(typeof(Collider2D))]
public class Icon : MonoBehaviour
{
    public Window window;


    float doubleClickTime = 1.0f;
    float lastClickTime = -10f;

    bool mouseHover;

    void Update()
    {
        if (!mouseHover)
            return;

        // Checking for double click
        if (Input.GetMouseButtonDown(0))
        {
            float timeDelta = Time.time - lastClickTime;

            if (timeDelta < doubleClickTime)
            {
                //Debug.Log("double click" + timeDelta);
                OpenWindow();
                lastClickTime = 0;
            }
            else
            {
                lastClickTime = Time.time;
            }
        }

    }

    void OpenWindow()
    {
        if (window.isOpen)
            WindowManager.instance.SetActiveWindow(window);

        if(window == null)
        {
            Debug.Log(name + " has no window assigned");
            return;
        }

        if (window.isOpen)
            return;

        window.Open();
    }
    
    void OnMouseOver()
    {
        //Debug.Log("Mouse hovering");
        mouseHover = true;
    }

    void OnMouseExit()
    {
        //Debug.Log("Mouse exited");
        mouseHover = false;
    }

}
