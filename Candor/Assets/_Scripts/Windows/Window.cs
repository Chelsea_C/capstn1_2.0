﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[RequireComponent(typeof(Collider2D))]
public class Window : MonoBehaviour
{
    
    public bool isOpen = false;
    public Vector3 scale;
    public Vector3 openPos;


    public bool moveable = true;
    
    Vector3 offset;
    [SerializeField]
    Vector3 previousPosition;
    Vector3 startingPosition;

    void Start()
    {
        startingPosition = transform.position;
        //LeanTween.scale(gameObject, new Vector3(.01f, .01f, .01f), 0f);
        //LeanTween.move(gameObject, icon.position, 0f);
        isOpen = false;
    }


    void Update()
    {
        if (isOpen)
        {
            return;
        }
    }

    void OnMouseDown()
    {
        WindowManager.instance.SetActiveWindow(this);
        offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
    }

    void OnMouseDrag()
    {
        if (!moveable)
            return;

        Vector3 newPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
        transform.position = Camera.main.ScreenToWorldPoint(newPosition) + offset;
    }

    public void Close()
    {
        previousPosition = gameObject.transform.position;
        LeanTween.move(gameObject, startingPosition, 0f);
        isOpen = false;
    }

    public void Open()
    {
        isOpen = true;
        WindowManager.instance.SetActiveWindow(this);

        if (previousPosition == Vector3.zero)
            LeanTween.move(gameObject, openPos, 0f).setEase(LeanTweenType.linear);
        else
            LeanTween.move(gameObject, previousPosition, 0f).setEase(LeanTweenType.linear);
    }
}
