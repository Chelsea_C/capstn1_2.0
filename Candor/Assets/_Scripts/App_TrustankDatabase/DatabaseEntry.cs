﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DATABASES
{
    [System.Serializable]
    public class DatabaseEntry
    {
        public string source;
        public string content;
        public string keywords;

        public DatabaseEntry(string _source, string _keywords, string _content)
        {
            source = _source;
            content = _content;
            keywords = _keywords;
        }
    }
}