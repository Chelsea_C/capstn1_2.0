﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace DATABASES
{ 
    public class DatabaseUI : MonoBehaviour
    {
        public Transform displayPosition;
        public GameObject searchPanel;
        public GameObject resultsPanel;
        public TMP_Text numberOfResults;

        Vector3 searchPanel_pos;
        Vector3 resultsPanel_pos;

        public bool searching;

        void Start()
        {
            searchPanel_pos = searchPanel.transform.position;
            resultsPanel_pos = resultsPanel.transform.position;
            ShowSearchPanel();
            searching = true;
        }
    
        void Update()
        {
            if (searching)
            {
                if (Input.GetKeyDown(KeyCode.KeypadEnter))
                    ShowResultsPanel();
            }
        }

        public void ShowSearchPanel()
        {
            searching = true;
            HidePanel(resultsPanel, resultsPanel_pos);
            
            LeanTween.move(searchPanel, displayPosition.position, 0f).setEase(LeanTweenType.linear);
        }

        public void ShowResultsPanel()
        {
            searching = false;
            HidePanel(searchPanel, searchPanel_pos);

            if (DatabaseManager.instance.results.Count < 1)
                numberOfResults.text = "No result(s) found for '" + DatabaseManager.instance.query + "'.";
            else
                numberOfResults.text = DatabaseManager.instance.results.Count.ToString() + " result(s) for '" + DatabaseManager.instance.inputField.text + "'." ;

            LeanTween.move(resultsPanel, displayPosition.position, 0f).setEase(LeanTweenType.linear);
        }

        void HidePanel(GameObject panel, Vector3 position)
        {
            LeanTween.move(panel, position, 0f).setEase(LeanTweenType.linear);
        }

        public void Search()
        {
            DatabaseManager.instance.SearchForQuery();
        }

        public void ClearSearch()
        {
            DatabaseManager.instance.ClearSearch();
        }

    }
}
