﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace DATABASES
{
    public class DatabaseManager : MonoBehaviour
    {
        #region Singleton
        public static DatabaseManager instance;

        void Awake()
        {
            instance = this;
        }
        #endregion

        public GameData database;

        public TMP_InputField inputField;

        public GameObject resultsUIPrefab;
        public List<DatabaseEntry> results;
        public Transform resultsParent;
        public Window trustank;

        public string query;
        DatabaseUI ui;
        
        void Start()
        {
            ui = trustank.GetComponent<DatabaseUI>();
            results = new List<DatabaseEntry>();
        }

        void Update()
        {
            query = inputField.text;

            if (WindowManager.instance.activeWindow != trustank)
                return;

            if (ui.searching)
            {
                if (Input.GetKeyDown(KeyCode.Return))
                    SearchForQuery();
            }

            if(!ui.searching)
            {
                if (Input.GetKeyDown(KeyCode.Backspace))
                    ClearSearch();
            }

        }

        public void SearchForQuery()
        {
            if (query == "")
                return;

            for (int i = 0; i < database.entries.Count; i++)
            {
                if (database.entries[i].keywords.Contains(query, StringComparison.OrdinalIgnoreCase))
                {
                    results.Add(database.entries[i]);
                }
            }
            GenerateResults();
            ui.ShowResultsPanel();
        }

        public void GenerateResults()
        {
            foreach(DatabaseEntry r in results)
            {
                GameObject result = Instantiate(resultsUIPrefab, resultsParent);
                DatabaseResultUI ui = result.GetComponent<DatabaseResultUI>();
                ui.AddEntry(r);
            }
        }


        public void ClearSearch()
        {
            ui.ShowSearchPanel();

            foreach(Transform child in resultsParent)
                GameObject.Destroy(child.gameObject);

            results.Clear();
            inputField.text = null;
            
        }
    }
}
