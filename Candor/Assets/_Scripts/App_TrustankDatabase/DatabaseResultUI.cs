﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace DATABASES
{
    public class DatabaseResultUI : MonoBehaviour
    {
        public Text source;
        public Text content;

        [SerializeField]
        DatabaseEntry entry;
        
        public void AddEntry(DatabaseEntry _entry)
        {
            entry = _entry;
            source.text = entry.source;
            content.text = entry.content;
        }
    }
}
