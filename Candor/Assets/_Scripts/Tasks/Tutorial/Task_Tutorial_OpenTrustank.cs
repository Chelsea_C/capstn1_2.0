﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task_Tutorial_OpenTrustank : Task
{
    public CandorUI candorWindow;
    public Window trustankWindow;

    bool trigger;
    bool trigger2;

    void Start()
    {
        trigger = true;
        trigger2 = true;
    }

    public override void CheckIfHappening()
    {
        if(trigger)
        {
            Debug.Log(name + " task active");

            EmailManager.instance.CreateEmail("Lillian Holloway", "Female6", "Last But Not Least", "This is way too easy for you so I'll skip to something more interesting. " + 
                "Check out the TrusTank database, I believe we pre-install that on all work laptops so it should be on your desktop somewhere.");

            //candorWindow.ShowIndicator();
            WindowManager.instance.CreatePopupNotificationWindow("Email Alert", "You have new mail");

            trigger = false;
        }

        #region Null Checks
        if (trustankWindow == null)
            Debug.Log("trustankWindow not set");
        #endregion

        if (trustankWindow.isOpen)
        {
            if (trigger2)
            {
                EmailManager.instance.CreateEmail("Lillian Holloway", "Female6", "About TrusTank", "Candor pays to have access to TrusTank, a collection " +
                    "of information from tried and trusted sources like Reuters, as reference for the fact-checking that we do for our clients. Use it wisely.");

                candorWindow.ShowIndicator();
                WindowManager.instance.CreatePopupNotificationWindow("Email Alert", "You have new mail");

                trigger2 = false;
            }

            TaskManager.instance.CompletedTask();
        }
    }
}
