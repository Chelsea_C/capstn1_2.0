﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task_Tutorial_OpenCandorEmail : Task
{
    public CandorUI candorWindow;
    public GameObject emailPanel;

    bool trigger;

    void Start()
    {
        
        trigger = true;
    }

    public override void CheckIfHappening()
    {
        if(trigger)
        {
            Debug.Log(name + " task active");

            EmailManager.instance.CreateEmail("Lillian Holloway", "Female6", "Welcome (Hopefully)", "Welcome to Candor! The team and I are " +
            "pleased to have you join us. With any luck, you've found the email feature of the program and are reading this. " +
            "Try looking for your task list now and completing the task there.");

            WindowManager.instance.CreatePopupNotificationWindow("Email Alert", "You have new mail");
            

            trigger = false;
        }

        #region Null Checks
        if(candorWindow == null)
        {
            Debug.Log("Candor window not assigned to " + name);
            return;
        }

        if(emailPanel == null)
        {
            Debug.Log("Email panel not assigned to " + name);
        }
        #endregion

        if (candorWindow.activePanel == emailPanel)
        {
            TaskManager.instance.CompletedTask();
        }
    }

}
