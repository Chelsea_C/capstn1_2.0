﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task_Tutorial_OpenFlaggedPosts : Task
{
    public CandorUI candorWindow;
    public GameObject flaggedPanel;

    bool trigger;

    void Start()
    {
        trigger = true;
    }

    public override void CheckIfHappening()
    {
        if (trigger)
        {
            Debug.Log(name + " task active");
            EmailManager.instance.CreateEmail("Lillian Holloway", "Female6", "Tasks are Friends",
                "You found the Task tab of the platform, great! Check back there for any special assignments.\n" +
                " As you know, we at Candor strive to be as thorough as we can in checking flagged posts for our clients.\n" +
                "In that effort, you will be assigned posts which will show up on your flagged posts tab. See if you can find the tab now.");

            //candorWindow.ShowIndicator();
            WindowManager.instance.CreatePopupNotificationWindow("Email Alert", "You have new mail");

            trigger = false;
        }

        #region Null Checks
        if (candorWindow == null)
        {
            Debug.Log("Candor window not assigned to " + name);
            return;
        }

        if (flaggedPanel == null)
        {
            Debug.Log("Email panel not assigned to " + name);
        }
        #endregion

        if (candorWindow.activePanel == flaggedPanel)
        {
            TaskManager.instance.CompletedTask();
        }
    }
}
