﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task_Tutorial_ReviewSamplePost : Task
{
    bool trigger;
    bool trigger2;
    bool trigger3;
    public Article sample;

    void Start()
    {
        trigger = true;
        trigger2 = false;
        trigger3 = false;
    }

    public override void CheckIfHappening()
    {
        if (trigger)
        {
            Debug.Log(name + " task active");

            EmailManager.instance.CreateEmail("Lillian Holloway", "Female6", "Flags are Fun",
                "Kudos once again, you are quite the achiever, finding the flagged post tab so quickly! This will" +
                " be filled with posts for you to mark as either true (Approve), fake (Reject) or an opinion (Opinion)." +
                "You may try it now with the sample I'll add for you.");

            WindowManager.instance.CreatePopupNotificationWindow("Email Alert", "You have new mail");

            sample = new Article(0, 2, "Lillian Holloway", "Candor is quickly growing to be world's best fact-checking organization.", " ");
            sample.active = true;

            trigger = false;
            trigger2 = true;
        }

        if(trigger2)
        {
            
            LevelManager.instance.AddTutorialArticle(sample);

            trigger2 = false;
            trigger3 = true;
        }

        if(trigger3)
        {
            if (sample.reviewed)
                trigger3 = false;
        }


        if (!trigger3 && !Player.instance.hasUnreadEmail())
            TaskManager.instance.CompletedTask();
    }
}
