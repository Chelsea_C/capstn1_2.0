﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task_Tutorial_OpenTasks : Task
{
    public CandorUI candorWindow;
    public GameObject tasksPanel;

    bool trigger;

    void Start()
    {
        trigger = true;
    }

    public override void CheckIfHappening()
    {
        if (trigger)
        {
            Debug.Log(name + " task active");

            trigger = false;
        }
        

        #region Null Checks
        if (candorWindow == null)
        {
            Debug.Log("Candor window not assigned to " + name);
            return;
        }

        if (tasksPanel == null)
        {
            Debug.Log("Email panel not assigned to " + name);
        }
        #endregion

        if (candorWindow.activePanel == tasksPanel)
        {
            TaskManager.instance.CompletedTask();
        }
    }
}
