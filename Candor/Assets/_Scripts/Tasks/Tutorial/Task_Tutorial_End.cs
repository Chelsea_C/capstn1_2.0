﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task_Tutorial_End : Task
{
    bool trigger;
    public GameObject endLevel;
    public GameObject skipTutorial;

    void Start()
    {
        trigger = true;
    }

    public override void CheckIfHappening()
    {
        if (trigger)
        {
            EmailManager.instance.CreateEmail("Lillian Holloway", "Female6", "You're all set!", "So to summarize: read flagged posts, do assigned tasks if any, research with the database, then mark the post (Approve/Reject/Opinion). Simple enough right?" +
        " Well I hope you're ready to try and save the world from post-truths, alternatives facts, and unsolicited opinions! Best of luck to you and again... Welcome to Candor!");

            WindowManager.instance.CreatePopupNotificationWindow("Email Alert", "You have new mail");

            trigger = false;
        }

        if (!trigger && !Player.instance.hasUnreadEmail())
        {
            endLevel.SetActive(true);
            skipTutorial.SetActive(false);
            Player.instance.tutorialComplete = true;
            TaskManager.instance.CompletedTask();
        } 
    }
}
