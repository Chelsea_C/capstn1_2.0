﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task_Tutorial_OpenHome : Task
{
    public CandorUI candorWindow;
    public GameObject homePanel;

    bool trigger;

    void Start()
    {
        trigger = true;
    }

    public override void CheckIfHappening()
    {
        if (trigger)
        {
            Debug.Log(name + " task active");

            EmailManager.instance.CreateEmail("Lillian Holloway", "Female6", "Home is Where the Stats Are",
                "Nice! Alright, we're almost done, I swear. The Home tab is next for you to check out, it's" +
                " where you'll find your profile and an overview of your stats on the tickets or posts that you've" +
                " reviewed.");

            WindowManager.instance.CreatePopupNotificationWindow("Email Alert", "You have new mail");

            trigger = false;
        }

        if (candorWindow.activePanel == homePanel)
            TaskManager.instance.CompletedTask();
    }
}
