﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task_Quota : Task
{
    public List<KeyCode> Keys;

    public override void CheckIfHappening()
    {
        
        for(int i = 0; i < Keys.Count; i++)
        {
            if (Input.GetKeyDown(Keys[i]))
            {
                Keys.RemoveAt(i);
            }
        }

        if(Keys.Count == 0)
        {
            TaskManager.instance.CompletedTask();
        }
    }


}
