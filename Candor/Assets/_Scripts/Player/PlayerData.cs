﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour
{
    public string playerName;
    public Sprite playerPortrait;
    public float score;
    public Rank rank;

    public bool tutorialComplete;

    public List<CheckedArticle> checkedArticles;

    public List<Email> emails;
    public List<Task> tasks;

    public PlayerData(Player player)
    {
        playerName = player.playerName;
        playerPortrait = player.playerPortrait;
        score = player.score;
        rank = player.rank;

        tutorialComplete = player.tutorialComplete;
        checkedArticles = player.checkedArticles;
        emails = player.emails;
        tasks = player.tasks;
    }

    public void ResetAll()
    {
        playerName = null;
        playerPortrait = null;
        score = 0f;
        tutorialComplete = false;

        checkedArticles.Clear();
        emails.Clear();
        tasks.Clear();
    }
}
