﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

    #region Singleton
    public static Player instance;

    void Awake()
    {
        instance = this;
    }
    #endregion

    public string playerName;
    public Rank rank;
    public Sprite playerPortrait;
    public float score;
    public float falsePercent;
    public float truePercent;
    public int level;
    
    public bool tutorialComplete = false;

    public List<CheckedArticle> checkedArticles;
    
    public List<Task> tasks;
    public List<Email> emails;

    public GameData database;

    void Start()
    {
        level = 1;
        score = 0;
        rank = Rank.JUNIOR_ANALYST;
        falsePercent = 0;
        truePercent = 0;
    }

    public void ApproveArticle(Article _article)
    {
        bool correct;
        if (_article.truth == 1)
            correct = true;
        else
            correct = false;

        CheckedArticle chArticle = new CheckedArticle(_article, Mark.Approved, correct);
        checkedArticles.Add(chArticle);

        _article.reviewed = true;
        database.articles.Remove(_article);

        //if (correct)
        //    score++;
        //else
        //    score--;

    }

    public void RejectArticle(Article _article)
    {
        bool correct;
        if (_article.truth == 0)
            correct = true;
        else
            correct = false;

        CheckedArticle chArticle = new CheckedArticle(_article, Mark.Rejected, correct);
        checkedArticles.Add(chArticle);

        _article.reviewed = true;
        database.articles.Remove(_article);

        //if (correct)
        //    score++;
        //else
        //    score--;
    }

    public void MarkArticleAsOpinion(Article _article)
    {
        bool correct;
        if (_article.truth == 2)
            correct = true;
        else
            correct = false;

        CheckedArticle chArticle = new CheckedArticle(_article, Mark.Opinion, correct);
        checkedArticles.Add(chArticle);

        _article.reviewed = true;
        database.articles.Remove(_article);

        //if (correct)
        //    score++;
        //else
        //    score--;
    }

    public void Save()
    {
        SaveSystem.SavePlayer(this);
    }

    public void Load()
    {
        PlayerData data = SaveSystem.LoadPlayer();

        playerName = data.playerName;
        playerPortrait = data.playerPortrait;
        score = data.score;
        tutorialComplete = data.tutorialComplete;
        checkedArticles = data.checkedArticles;
        emails = data.emails;
        tasks = data.tasks;
    }

    public bool hasUnreadEmail()
    {
        for(int i = 0; i< emails.Count; i++)
        {
            if (!emails[i].read)
                return true;
                
        }
        return false;
    }

}
