﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Mark
{
    Approved,
    Rejected,
    Opinion
}

[System.Serializable]
public class CheckedArticle
{
    public Article article;
    public Mark mark;
    public bool correct;

    public CheckedArticle(Article _article, Mark _mark, bool _correct)
    {
        article = _article;
        mark = _mark;
        correct = _correct;
    }
}
